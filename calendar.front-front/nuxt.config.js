
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'calendar',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  css: [
    // '@/assets/fa/scss/fontawesome.scss',
    // { src: '@/assets/fa/scss/fontawesome.scss', lang: 'scss' },
    // { src: '@/assets/fa/scss/fa-brands.scss', lang: 'scss' },
    { src: '@/assets/fa/scss/fontawesome.scss', lang: 'scss' },
    { src: '@/assets/fa/scss/fa-solid.scss', lang: 'scss' },
    { src: '@/assets/scss/index.scss', lang: 'scss' },
    
  ],
  plugins: [
    '~/plugins/moment',
    { src: '~/plugins/local-storage', ssr: false },
    '~/plugins/lodash',
    { src: '~/plugins/apollo', ssr: false},

  ]
  ,
  modules: [
    'nuxt-sass-resources-loader',
    ['@nuxtjs/apollo', {
      clientConfigs: {
        default: '~/plugins/apollo.js',
      },
    }],
  ],
  sassResources: [
     '@/assets/scss/index.scss'
  ],
  build: {
    postcss: {
      plugins: {
        'postcss-custom-properties': false
      }
    },
    /*
    ** Run ESLint on save
    */
    vendor: ['axios', 'lodash', 'moment'],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

