import Moment from 'moment';
import { extendMoment } from 'moment-range';
import Vue from 'vue';

Vue.set(Vue.prototype, 'moment' , extendMoment(Moment));
