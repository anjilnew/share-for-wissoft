import lodash from 'lodash'
import Vue from 'vue';

Vue.set(Vue.prototype, 'lodash', lodash)